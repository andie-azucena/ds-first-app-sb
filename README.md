# DataSonnet in Spring Boot

This is a simple API that transforms a list of characters from Lord of the Rings from one format to another using DataSonnet.

DataSonnet Basics document can be found [here](https://docs.google.com/document/d/1pmnMvCrSEjeuXYydBsgy3VPmqBFOyk_wx2i66V38PsQ/edit?usp=sharing)

## Technologies

* Java 8+
* Maven
* Spring Boot
* DataSonnet Mapper

## How to Run

Once the project is cloned, there are 2 ways:

Using IDE: 
* Import the cloned project. 
* Once Maven updates the project and downloads all dependencies, right-click on the project and run as Spring Boot App.

Using Terminal:
* Run `mvn spring-boot:run`

## How to Test

Using Postman, go to [http://localhost:8080/heroes](http://localhost:8080/heroes)

## DataSonnet Playground

To test the mocked data using the DataSonnet script in transformation.ds, go to [DS Playground](https://datasonnet.ms3-inc.com/)

Input Payload:

```
[
  {
    "id": 1,
    "name": "Frodo Baggins",
    "race": "Hobbit"
  },
  {     
    "id": 2,
    "name": "Samwise Gamgee",
    "race": "Hobbit"
  },
  {     
    "id": 3,
    "name": "Legolas Greenleaf",
    "race": "Sindarin Elf"
  }
]
```

Script:

```
/** DataSonnet
version=2.0
output application/json
input payload application/json
*/
ds.map(payload, function(hero,index) {
    "firstName" : ds.splitBy(hero.name, " ")[0],
    "lastName" : ds.splitBy(hero.name, " ")[1],
    "race" : hero.race
})
```

Output:

```
[
  {
    "firstName": "Frodo",
    "lastName": "Baggins",
    "race": "Hobbit"
  },
  {
    "firstName": "Samwise",
    "lastName": "Gamgee",
    "race": "Hobbit"
  },
  {
    "firstName": "Legolas",
    "lastName": "Greenleaf",
    "race": "Sindarin Elf"
  }
]
```
