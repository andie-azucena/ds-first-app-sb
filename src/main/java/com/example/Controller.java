package com.example;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datasonnet.Mapper;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class Controller {

	@Value("classpath:transformation.ds")
	private Resource resource;	
	
	@GetMapping(path="/heroes", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getData() throws Exception {
		
		// Dummy data
		Hero h1 = new Hero(1, "Frodo Baggins", "Hobbit");
		Hero h2 = new Hero(2, "Samwise Gamgee", "Hobbit");
		Hero h3 = new Hero(3, "Legolas Greenleaf", "Sildarin Elf");
		List<Hero> heroes = Arrays.asList(h1, h2, h3);
		
		// Convert list to json array
		ObjectMapper jsonMapper = new ObjectMapper();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		jsonMapper.writeValue(out, heroes);
		byte[] data = out.toByteArray();
		
		// OPTION 1: Using a hardcoded DS script
		
		// Uses DataSonnet library
//		String datasonnetScript = 
//				// header
//				"/** DataSonnet \n" +
//				"version=2.0 \n" +
//				"output application/json \n" +
//				"input payload application/json \n" +
//				"*/ \n" +
//				
//				// body
//				"ds.map(payload, function(hero)\n" +
//                "{\n" +
//                "  firstName: ds.splitBy(hero.name, \" \")[0], \n" +
//                "  lastName: ds.splitBy(hero.name, \" \")[1], \n" +
//                "  race: hero.race\n" +
//                "})";
//		
//		Mapper mapper = new Mapper(datasonnetScript);

		//	OPTION 2: Using an external DS file
		// Convert data using mapping script in transformation.ds
		Mapper mapper = new Mapper(resourceToString(this.resource));
//		
		// Transform the heroes list data
		String transformedResponse = mapper.transform(new String(data));
		
		return ResponseEntity.ok(transformedResponse);
	}
	
    private static String resourceToString(Resource resource) {
        try (Reader reader = 
        		new InputStreamReader(resource.getInputStream(), 
        				Charset.defaultCharset())) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }	
}
