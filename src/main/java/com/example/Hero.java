package com.example;

public class Hero {

	private Integer id;
	private String name;
	private String race;
	
	public Hero(Integer id, String name, String race) {
		super();
		this.id = id;
		this.name = name;
		this.race = race;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRace() {
		return race;
	}
	public void setRace(String race) {
		this.race = race;
	}
}
